<?php
/**
 * created by jamy
 * blog: https://www.imjamy.com
 */

define('TEMP_CHECK','temp');
define('ROOT',__DIR__);

require_once '../vendor/autoload.php';
require_once '../core/boot.php';