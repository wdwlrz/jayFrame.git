<?php
return [
    'debug'=>true,

    // url模式，1为伪静态，0为普通。尽量不要在url中使用s参数，解析url时会销毁此格式（s=/xxx）的s参数，即斜杠开头的参数值。
    'url_mode' => 1,

    'template' => [
        // 模板文件目录
        'view_path'   => dirname(__DIR__).'/app/'.$GLOBALS['request']['m'].'/view/'.$GLOBALS['request']['c'].'/',
        // 模板编译缓存目录（可写）
        'cache_path'  => '../runtime/template/',
        // 模板文件后缀
        'view_suffix' => 'html',
        // 模板字符串替换
        'tpl_replace_string' => [
            '__CSS__'=>'/public/css',
            '__JS__'=>'/public/js',
        ],
    ],

    'log' => [
        // 默认日志记录通道
        'default'      => 'file',
        // 日志记录级别
        'level'        => [],
        // 日志通道列表
        'channels'     => [
            'file' => [
                // 日志记录方式
                'type'        => 'File',
                // 日志保存目录
                'path'        => dirname(__DIR__).'/runtime/log/',
                // 单文件日志写入
                'single'      => false,
                // 独立日志级别
                'apart_level' => [],
                // 最大日志文件数量
                'max_files'   => 0,
            ],
            // 其它日志通道配置
        ]
    ]
];