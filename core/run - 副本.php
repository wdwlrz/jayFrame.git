<?php
/**
 * created by jamy
 * blog: https://www.imjamy.com
 */

runStart();

/********************** funcs **********************/

function runStart(){
    // 初始化日志模块
    initLog(config('log'));

    recordLog('------------------------------------------------------------','info');
    $url=getPageUrl();
    $ip_method_url=getclientip()." ".strtoupper($_SERVER['REQUEST_METHOD'])." ".$url;
    recordLog($ip_method_url,'info');
    recordLog(' [ URL ] '.$url,'info');

    $header=getallheaders();
    recordLog(' [ HEADER ] '.json_encode($header,JSON_UNESCAPED_UNICODE),'info'); // var_export($header,true) 也可以，json的方便程序分析


    $m='';
    $c='';
    $a='';
    $GLOBALS['request']=[];

    $config=require(dirname(__DIR__).'/conf/config.php');

    if ($config['debug']==false){
        error_reporting(0);
    }

    if ($config['url_mode']==1 && isset($_GET['s']) && $_GET['s']!=''){
        $s=$_GET['s'];
        if (substr($s,strlen($s)-1,1)=='/'){
            $s=substr($s,0,strlen($s)-1);
        }
        if (substr($s,0,1)!='/'){
            $s='/'.$s;
        }
        $route=explode('/',$s);
        if ($route){
            array_shift($route);
            $params=$route;
        }

        // 处理模块、控制器、方法
        if (count($route)==0){
            $m=$c=$a='index';
        }
        if (count($route)==1){
            $m=$route[0];
            $c=$a='index';
        }
        if (count($route)==2){
            $m='index';
            $c=$route[0];
            $a=$route[1];
        }
        if (count($route)==3){
            $m=$route[0];
            $c=$route[1];
            $a=$route[2];
        }
        if (count($route)>=2 && count($route)%2==0){
            // 默认index模块
            $m='index';
            $c=$route[0];
            $a=$route[1];
            unset($params[0]);
            unset($params[1]);
        }
        if (count($route)>=3 && count($route)%2==1){
            // 默认index模块
            $m=$route[0];
            $c=$route[1];
            $a=$route[2];
            unset($params[0]);
            unset($params[1]);
            unset($params[2]);
        }

        // 处理后缀
        $a_arr=explode('.',$a);
        if (count($a_arr)>1){
            $suffix=$a_arr[count($a_arr)-1];
            if (!in_array($suffix,['html','htm'])){
                runEnd(0,'','方法名无法识别');
            }else{
                $a=$a_arr[0];
            }
        }

        // 处理参数
        if (count($params)>=2){
            $params=array_values($params);
            for ($i = 0; $i < count($params)-1; $i++) {
                $_GET[$params[$i]]=$params[$i+1];
            }
        }
        unset($_GET['s']);


    }else{

        if (!isset($_GET['m'])){
            $m='index';
        }else{
            $m=$_GET['m'];
        }

        if (!isset($_GET['c'])){
            $c='index';
        }else{
            $c=$_GET['c'];
        }

        if (!isset($_GET['a'])){
            $a='index';
        }else{
            $a=$_GET['a'];
        }
    }

    recordLog('[ ROUTE ] '.json_encode([$m,$c,$a],JSON_UNESCAPED_UNICODE),'info');
    recordLog('[ GetParams ] '.json_encode($_GET,JSON_UNESCAPED_UNICODE),'info');
    recordLog('[ PostParams ] '.json_encode($_POST,JSON_UNESCAPED_UNICODE),'info');

    $GLOBALS['request']['m']=$m;
    $c=str_replace(".","\\",$c);// 控制器在子目录中
    $GLOBALS['request']['c']=$c;
    $GLOBALS['request']['a']=$a;

    if ($m==''){
        runEnd(0,'','缺少模块参数');
    }

    if ($c==''){
        runEnd(0,'','缺少控制器参数');
    }

    if ($a==''){
        runEnd(0,'','缺少方法参数');
    }


// 加载控制器
    $controller_path=dirname(__DIR__).'/app/'.$m.'/controller/'.$c.'.php';
    $controller_exist=file_exists($controller_path);
    if ($controller_exist){
        try{
            $controller_namespace="\app\\$m\controller\\";
            $controller_class=$controller_namespace.$c;
            if (class_exists($controller_class)==false){
                // class_exists 会自动调用我们注册好的自动加载机制
                runEnd(-3,'',"控制器中 $controller_class 类 不存在，请检查命名空间和类名");
            }else{
                $controller=new $controller_class(); // 使用命名空间自动加载，无需提前引入文件。
            }

            if (method_exists($controller,$a)==false){
                runEnd(-4,"","控制器中 $a 方法 不存在");
            }
            $return=$controller->$a();
            runEnd($return);
        }catch (\Exception $e){
            runEnd(-1,'','抛出异常，内容：'.$e->getMessage());
        }

    }else{
        runEnd(-2,'','控制器不存在');
    }
}


function runEnd(...$arr){
    if (count($arr)==3){
        recordLog($arr[2],'error');
        $return=json($arr[0],$arr[1],$arr[2]);
        $die=1;
    }else{
        $return=$arr[0];
        $die=0;
    }

    // 记录sql日志
    recordLogBatch(getSqlLog(),'sql');

    saveLog();

    if ($die){
        die($return);
    }else{
        echo $return;
    }
}

