<?php
namespace core;

$autoload=new autoload_class();
$autoload->register();

class autoload_class{
    function load($class){
        $file=dirname(__DIR__).'/'.str_replace("\\",'/',$class).'.php';

        if (file_exists($file)){
            require_once $file;
        }else{
            $file=str_replace("/","\\",$file);
            exception($file. " 未找到此文件，自动加载失败",-5);
//            runEnd(-5,'',$file. " 未找到此文件，自动加载失败");
        }
    }

    function register(){
        spl_autoload_register("self::load");
    }
}

