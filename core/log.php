<?php
use think\facade\Log;

/**
 * 初始化日志模块
 */
function initLog(array $config){
    Log::init($config);
}

/**
 * 记录日志信息
 * @access public
 * @param  mixed  $msg       日志信息
 * @param  string $type      日志级别
 * @param  array  $context   替换内容
 */
function recordLog($msg, string $type = 'info', array $context = []){
    static $id;
    if ($msg=='' || $msg==null){
        $msg="[ null ]";
    }
    if (is_array($msg)){
        $msg=json_encode($msg,JSON_UNESCAPED_UNICODE);
        $msg='[ ArrayToJson ] '.$msg;
    }
//    $id++;
//    $msg="[$id]: ".$msg;
    Log::record($msg,$type,$context);
}

/**
 * 批量记录日志
 * @param array $msgArr
 * @param string $type
 */
function recordLogBatch(array $msgArr, string $type='info', array $context = []){
    foreach ($msgArr as $item) {
        recordLog($item,$type,$context);
    }
}

/**
 * 保存日志
 */
function saveLog(){
    Log::save();
}
