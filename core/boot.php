<?php
/**
 * created by jamy
 * blog: https://www.imjamy.com
 */

$GLOBALS['runtime_time']=microtime(true);

require_once dirname(__DIR__).'/inc/functions.php';
require_once __DIR__.'/functions.php';
require_once __DIR__.'/helper.php';
require_once __DIR__.'/log.php';
require_once __DIR__.'/db.php';
require_once __DIR__.'/controller.php';
require_once __DIR__.'/view.php';
require_once __DIR__.'/autoload.php';
require_once __DIR__.'/run.php';

