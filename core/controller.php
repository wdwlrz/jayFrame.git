<?php
namespace core;
use think\Template;
abstract class Controller
{
    // 模板
    public $template;


    public function __construct()
    {
        $this->_init_controller();
        $this->__init__();
    }

    public function _init_controller()
    {
        // 模板
        $this->template=new Template(config('template'));
    }

    public function __init__()
    {
        // 控制器子类中初始化函数使用这个
    }


    /**
     * 模板渲染
     * @param $file
     * @param array $var
     */
    public function fetch($file,$var=[]){
        $this->template->fetch($file,$var);
    }

    /**
     * 模板赋值
     * @param array $vars
     */
    public function assign($vars = []){
        $this->template->assign($vars);
    }




    protected function success($msg='',$data=[])
    {
        die(json(1,$data,$msg));
    }

    protected function error($msg='',$data=[])
    {
        die(json(0,$data,$msg));
    }

}

