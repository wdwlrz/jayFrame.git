<?php

use think\Template;

/**
 * 简单模板输出
 * @param string $file
 * @param array $var
 */
function view_($file='', $var=[]){
    // 不要跨控制器使用view，比如在A类里调用B类的b1方法内使用view(),这时输出的是A控制器目录下的模板，已经跟url中的控制器绑定了。
    if ($file==''){
        $backtrace = debug_backtrace(false,2);
        array_shift($backtrace);
        $file=$backtrace[0]['function'];
    }
    if ($var && is_array($var)){
        foreach ($var as $k=>$v) {
            $$k=$v;
        }
    }

    $controller_path=dirname(__DIR__).'/app/'.$GLOBALS['request']['m'].'/view/'.$GLOBALS['request']['c'].'/'.$file.'.html';
    $controller_exist=file_exists($controller_path);
    if ($controller_exist){
        require($controller_path);
    }else{
        die(json(0,'模板不存在'));
    }
}


/**
 * think-template 模板渲染
 */
if (!function_exists('view')) {
    /**
     * 模板渲染输出助手函数
     * @param string $file
     * @param array $var
     */
    function view($file='',$var=[]){
        if ($file==''){
            $backtrace = debug_backtrace(false,2);
            array_shift($backtrace);
            $file=$backtrace[0]['function'];
        }
        $config=config('template');
        $template = new Template($config);
        // 模板变量赋值
        $template->assign($var);
        // 读取模板文件渲染输出
        $template->fetch($file);
    }
}


