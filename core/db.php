<?php
use think\Db;
define('IS_CLI', PHP_SAPI == 'cli' ? true : false);

$dbConfig=config('','db');

$GLOBALS['_dbConfig']=$dbConfig;

Db::setConfig($dbConfig);

/**
 * think-orm
 */
if (!function_exists('db')) {
    /**
     * 实例化数据库类
     * @param string        $name 操作的数据表名称（不含前缀）
     * @param array|string  $config 数据库配置参数
     * @param bool          $force 是否强制重新连接
     * @return \think\db\Query
     */
    function db($name = '', $config = [], $force = false)
    {
        return Db::connect($config, $force)->name($name);
    }
}

if (!function_exists('query')) {
    /**
     * query 助手函数 可替换前缀
     * 执行查询 返回数据集
     * @access public
     * @param string      $sql    sql指令
     * @param array       $bind   参数绑定
     * @param boolean     $master 是否在主服务器读操作
     * @param bool|string $class  指定返回的数据集对象
     * @return mixed
     * @throws BindParamException
     * @throws PDOException
     */
    function query($sql, $bind = [], $master = false, $class = false)
    {
        $sql=str_replace("[P]",$GLOBALS['_dbConfig']['prefix'],$sql);
        return Db::query($sql, $bind, $master, $class);
    }
}

if (!function_exists('getSqlLog')) {
    function getSqlLog()
    {
        return Db::getSqlLog();
    }
}