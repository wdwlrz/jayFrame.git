<?php


if (!function_exists('dump')) {
    /**
     * 浏览器友好的变量输出
     * @access public
     * @param mixed $var 变量
     * @param boolean $echo 是否输出(默认为 true，为 false 则返回输出字符串)
     * @param string|null $label 标签(默认为空)
     * @param integer $flags htmlspecialchars 的标志
     * @return null|string
     */
    function dump($var, $echo = true, $label = null, $flags = ENT_SUBSTITUTE)
    {
        $label = (null === $label) ? '' : rtrim($label) . ':';

        ob_start();
        var_dump($var);
        $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', ob_get_clean());

        if (IS_CLI) {
            $output = PHP_EOL . $label . $output . PHP_EOL;
        } else {
            if (!extension_loaded('xdebug')) {
                $output = htmlspecialchars($output, $flags);
            }

            $output = '<pre>' . $label . $output . '</pre>';
        }

        if ($echo) {
            echo($output);
            return;
        }

        return $output;
    }
}

if (!function_exists('json')) {
    function json(...$par){
        header("content:application/json;chartset=uft-8");
        if (count($par)>1){
            $data['code']=$par[0];
            if (count($par)>=2){
                $data['data']=$par[1];
            }
            if (count($par)==3){
                $data['msg']=$par[2];
            }
            $json = json_encode($data,JSON_UNESCAPED_UNICODE);
        }else{
            if (is_array($par[0]) || is_object($par[0])){
                $json = json_encode($par[0],JSON_UNESCAPED_UNICODE);
            }else{
                $json = json_encode(['code'=>1,'data'=>$par[0]]);
            }

        }
        return $json;
    }
}



if (!function_exists('input')) {
    function input($p=''){
        if ($p==''){
            $method=$_SERVER['REQUEST_METHOD'];
            switch (strtolower($method)){
                case 'get':
                    return $_GET;
                case 'post':
                    return $_POST;
                default:
                    return $_POST;
            }
        }
        $p=explode('.',$p);
        if (count($p)==1){
            $pp=$p[0];
            if (isset($_GET[$pp])){
                return $_GET[$pp];
            }
            if (isset($_POST[$pp])){
                return $_POST[$pp];
            }
        }
        if (count($p)==2){
            if ($p[1]==''){
                switch (strtolower($p[0])){
                    case 'get':
                        return $_GET;
                    case 'post':
                        return $_POST;
                    case 'put':
                        // 未测试
                        parse_str(file_get_contents('php://input'), $input);
                        return $input;
                    default:
                        return $_POST;
                }
            }else{
                if ($p[0]=='get'){
                    if (isset($_GET[$p[1]])){
                        return $_GET[$p[1]];
                    }else{
                        return '';
                    }
                }
                if ($p[0]=='post'){
                    if (isset($_POST[$p[1]])){
                        return $_POST[$p[1]];
                    }else{
                        return '';
                    }
                }
            }
        }
        return '';
    }
}


if (!function_exists('redirect')) {
    function redirect($url)
    {
        header("Location: $url");
        exit;
    }
}


if (!function_exists('session')) {
    function session(...$a)
    {
        if (count($a)==0){
            return $_SESSION;
        }

        $k=$a[0];
        $ks=explode('.',$k);
        if (count($ks)==2 && $ks[1]!=''){
            $s1=$ks[0];
            $s2=$ks[1];
        }else{
            $s1=$ks[0];
        }

        if (count($a)==1){
            if (!isset($s2)){
                if (isset($_SESSION[$s1])){
                    return $_SESSION[$s1];
                }else{
                    return null;
                }
            }else{
                if (isset($_SESSION[$s1][$s2])){
                    return $_SESSION[$s1][$s2];
                }else{
                    return null;
                }
            }
        }
        if (count($a)>=2){
            $v=$a[1];
            if ($v!==null){
                if (isset($s2)){
                    return $_SESSION[$s1][$s2]=$v;
                }else{
                    return $_SESSION[$s1]=$v;
                }
            }else{
                if (isset($s2)){
                    unset($_SESSION[$s1][$s2]);
                }else{
                    unset($_SESSION[$s1]);
                }
            }
        }
    }
}


if (!function_exists('config')) {
    function config($key='',$configFile=''){
        if ($configFile==''){
            $configFile='config';
        }
        $configFile=dirname(__DIR__).'/conf/'.$configFile.'.php';
        $config=require($configFile);
        if ($key==''){
            return $config;
        }else{
            if (isset($config[$key])){
                return $config[$key];
            }else{
                return null;
            }
        }
    }
}


if (!function_exists('exception')){
    function exception($message = "", $code = 0, Throwable $previous = null){
        throw new \Exception($message, $code, $previous);
    }
}







